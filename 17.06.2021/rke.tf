resource "hcloud_ssh_key" "default" {
  name       = "Terraform Example"
  public_key = file("~/.ssh/id_rsa.pub")
}

data "template_file" "cloudconf" {
  template = file("./config/cloud-config.yml")
  vars = {
    "custom_ssh" = hcloud_ssh_key.default.public_key
  }
}

resource "hcloud_server" "master" {
  count       = 3
  name        = "master${count.index + 1}"
  image       = "ubuntu-20.04"
  server_type = "cpx31"
  location    = "fsn1"
  ssh_keys    = [hcloud_ssh_key.default.id]
  user_data   = data.template_file.cloudconf.rendered
  depends_on = [
    hcloud_ssh_key.default
  ]
}

resource "hcloud_server" "node" {
  count       = 3
  name        = "node${count.index + 1}"
  image       = "ubuntu-20.04"
  server_type = "cpx31"
  location    = "fsn1"
  ssh_keys    = [hcloud_ssh_key.default.id]
  user_data   = data.template_file.cloudconf.rendered
  depends_on = [
    hcloud_ssh_key.default
  ]
}

resource "rke_cluster" "pepega" {
  cluster_name = "pepega"
  dynamic "nodes" {
    for_each = hcloud_server.master
    content {
      address = nodes.value.ipv4_address
      user    = "root"
      role    = ["etcd", "controlplane"]
    }
  }
  dynamic "nodes" {
    for_each = hcloud_server.node
    content {
      address = nodes.value.ipv4_address
      user    = "root"
      role    = ["worker"]
    }
  }
}
resource "local_file" "k8s_confing" {
  file_permission   = "600"
  filename          = "./rke_kube_config.yml"
  sensitive_content = rke_cluster.pepega.kube_config_yaml
}
output "master_ip" {
  value = zipmap(hcloud_server.master.*.name, hcloud_server.master.*.ipv4_address)
}

