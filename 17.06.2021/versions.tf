terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.26.2"
    }
    rke = {
      source  = "rancher/rke"
      version = "1.2.2"
    }
    local = {
      source = "hashicorp/local"
    }
    template = {
      source = "hashicorp/template"
    }
  }
  required_version = ">= 0.15"
}

provider "hcloud" {
  token = var.hcloud_token
}

provider "rke" {
  log_file = "rke_debug.log"

}
