# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.26.2"
  constraints = "1.26.2"
  hashes = [
    "h1:vNjOQKUyWQkVs4UjWAgowBT7j/l51ffELd3qgEZ4QIM=",
    "zh:17cd6983c85e6dcc0e70bac525d29b9aedd09805fd90214aa556c84d0af13061",
    "zh:2b28d90522a91d91bb0b189317c34b8374d8c479e7a2b9ca0f6d0d1d14286303",
    "zh:35581626f3b700897fbe1697ddc8ff274079b7e2b6aefc20ca2316f621e212d5",
    "zh:3a7a2dc5a85957ae0c1e2608c5164f3d6cbc5be78cc343231c3a57307e839c0f",
    "zh:592eabdc6486bdcfa724cfe86f374b8ee9a416f3489704af99ee3a3901dfa973",
    "zh:62818a42119f6359cdf581c9d00d1d2d4acfda11a1b362a747963886ed226ba5",
    "zh:8d3b70ddb4ed0242a0ed16013b991314fe7793afd26155145dcd5f353381b493",
    "zh:9188e9aecd9407e25fa7f5655eb1eaab7ae6960a8407357c968d9cca69f50805",
    "zh:bd00e893950b70e1c950d19249febbf8fb9c4c2d47f4414bc5c256a5c0d4148c",
    "zh:c7329d5fe20bd4cf2975c0aafb31ae722b4bb092ee654a31261bd8c080de0127",
    "zh:ddd2d132fc0d1a8169e4c4d95e19b753ed4c9bc8a6d2fbc045c436a530638c69",
    "zh:e28807a7c2f9a4975638fa7a12b9f0525396cccf5b86d1c2dd00f6f073cc3168",
    "zh:ec828709926eadf2f2f1526d1bd8304435ef42875cf5b4c7005854b8206c5a34",
  ]
}

provider "registry.terraform.io/rancher/rke" {
  version     = "1.2.2"
  constraints = "1.2.2"
  hashes = [
    "h1:cVYPcMtfQ/Upt507hlQU2oC6aKsRGvx66IOOk8UeSR4=",
    "zh:2c69a6b2c076ceabf08312f25f5042be5afc0264d9d2f063d9ec6a3dd12205ed",
    "zh:58ab4aa01574c8ca1a4b28b3bbc1f4501abf3d5dc1d51ed7120b5e41b3ec191f",
    "zh:5c489814801acd28721f106b633c485595cb615c620a816dcc1eae108a4fdab4",
    "zh:8c5a3f28309a2be9f9744eb0852bc2746bd53582940aff07550f887c200dc929",
    "zh:9baebd8b619dea744a57eb3129c5d0e766bc14df5f207d465409f33de3e423c9",
    "zh:b6a33af13310a17c44fdd14a5255582c78e0c80e58c26b1879a36a6549112387",
  ]
}
