variable "server_image" {
  default = "ubuntu-20.04"
}

variable "location" {

  default = "hel1"

}

variable "master_type" {

  default = "cpx21"

}

variable "worker_type" {

  default = "cpx31"

}

variable "hcloud_token" {

}