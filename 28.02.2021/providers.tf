terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.24.1"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/24753747/terraform/state/hcloud_k8s_tutorial"
    lock_address   = "https://gitlab.com/api/v4/projects/24753747/terraform/state/hcloud_k8s_tutorial/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/24753747/terraform/state/hcloud_k8s_tutorial/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }
}
