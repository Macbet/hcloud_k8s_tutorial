resource "hcloud_server" "master" {
  count       = 3
  name        = "master-${count.index}"
  image       = var.server_image
  server_type = var.master_type
  location    = var.location
  ssh_keys    = [data.hcloud_ssh_key.k8s-key.id]
  network {
    network_id = hcloud_network.privateNetwork.id
  }
  depends_on = [ hcloud_network_subnet.k8s]
}

resource "hcloud_server" "worker" {
  count       = 3
  name        = "worker-${count.index}"
  image       = var.server_image
  server_type = "cpx31"
  location    = var.location
  ssh_keys    = [data.hcloud_ssh_key.k8s-key.id]
  network {
    network_id = hcloud_network.privateNetwork.id
  }
  depends_on = [ hcloud_network_subnet.k8s]

}