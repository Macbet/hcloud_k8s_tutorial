resource "hcloud_network" "privateNetwork" {

  name     = "privateNetwork"
  ip_range = "10.0.0.0/8"
  labels = {
    "k8s" = "true"
  }
  lifecycle {
    prevent_destroy = true
  }
}


resource "hcloud_network_subnet" "k8s" {
  network_id   = hcloud_network.privateNetwork.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = "10.0.1.0/24"
  lifecycle {
    prevent_destroy = true
  }
  depends_on = [hcloud_network.privateNetwork]

}