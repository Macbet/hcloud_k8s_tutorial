data "digitalocean_image" "fedora-coreos" {
  name = "fedora-coreos-34.20210529.3.0"
}

module "nemo" {
  source = "./modules/digital-ocean/fedora-coreos/kubernetes"

  cluster_name     = "nemo"
  region           = "ams3"
  dns_zone         = "pepegalabs.com"
  os_image         = data.digitalocean_image.fedora-coreos.id
  ssh_fingerprints = ["3c:f1:d1:93:44:47:71:21:fd:73:d9:43:93:ef:95:65"]
  networking       = "cilium"
  # optional
  worker_count = 3
}


resource "local_file" "kubeconfig-nemo" {
  content  = module.nemo.kubeconfig-admin
  filename = "/home/macbet/.kube/configs/nemo-config"
}
